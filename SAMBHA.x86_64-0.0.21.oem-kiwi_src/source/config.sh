#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$name]..."

#======================================
# SuSEconfig
#--------------------------------------
echo "** Running suseConfig..."
suseConfig

echo "** Running ldconfig..."
/sbin/ldconfig

#======================================
# Setup default runlevel
#--------------------------------------
baseSetRunlevel 3

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey


#======================================
# WebYast Configuration
#--------------------------------------
echo '** Configuring WebYast...'
insserv webyast

#======================================
# RPM GPG Keys Configuration
#--------------------------------------
echo '** Importing GPG Keys...'
rpm --import /studio/studio_rpm_key_0
rm /studio/studio_rpm_key_0

sed --in-place -e 's/# solver.onlyRequires.*/solver.onlyRequires = true/' /etc/zypp/zypp.conf

# Enable sshd
chkconfig sshd on

#======================================
# Sysconfig Update
#--------------------------------------
echo '** Update sysconfig entries...'
baseUpdateSysConfig /etc/sysconfig/keyboard KEYTABLE us.map.gz
baseUpdateSysConfig /etc/sysconfig/network/config FIREWALL no
baseUpdateSysConfig /etc/init.d/suse_studio_firstboot NETWORKMANAGER no
baseUpdateSysConfig /etc/sysconfig/SuSEfirewall2 FW_SERVICES_EXT_TCP 22\ 80\ 443\ 4984
baseUpdateSysConfig /etc/sysconfig/console CONSOLE_FONT lat9w-16.psfu


#======================================
# Setting up overlay files 
#--------------------------------------
echo '** Setting up overlay files...'
mkdir -p /root/
mv /studio/overlay-tmp/files//root//hot-scripts.tar /root//hot-scripts.tar
chown nobody:nobody /root//hot-scripts.tar
chmod 644 /root//hot-scripts.tar
mkdir -p /
mv /studio/overlay-tmp/files///samba-config.tar.bz2 //samba-config.tar.bz2
chown root:root //samba-config.tar.bz2
chmod 644 //samba-config.tar.bz2
mkdir -p /root/
mv /studio/overlay-tmp/files//root//fs_init.sh /root//fs_init.sh
chown nobody:nobody /root//fs_init.sh
chmod 644 /root//fs_init.sh
mkdir -p /root/
mv /studio/overlay-tmp/files//root//fs_sanitycheck.sh /root//fs_sanitycheck.sh
chown nobody:nobody /root//fs_sanitycheck.sh
chmod 644 /root//fs_sanitycheck.sh
mkdir -p /root/
mv /studio/overlay-tmp/files//root//fs_synchaconfig.sh /root//fs_synchaconfig.sh
chown nobody:nobody /root//fs_synchaconfig.sh
chmod 644 /root//fs_synchaconfig.sh
mkdir -p /etc/
mv /studio/overlay-tmp/files//etc//issue /etc//issue
chown nobody:nobody /etc//issue
chmod 644 /etc//issue
mkdir -p /etc/sysconfig/
mv /studio/overlay-tmp/files//etc/sysconfig//SuSEfirewall2 /etc/sysconfig//SuSEfirewall2
chown root:root /etc/sysconfig//SuSEfirewall2
chmod 666 /etc/sysconfig//SuSEfirewall2
mkdir -p /etc/sysconfig/
mv /studio/overlay-tmp/files//etc/sysconfig//syslog /etc/sysconfig//syslog
chown root:root /etc/sysconfig//syslog
chmod 644 /etc/sysconfig//syslog
test -d /studio || mkdir /studio
cp /image/.profile /studio/profile
cp /image/config.xml /studio/config.xml
rm -rf /studio/overlay-tmp
true

#======================================
# SSL Certificates Configuration
#--------------------------------------
echo '** Rehashing SSL Certificates...'
c_rehash
