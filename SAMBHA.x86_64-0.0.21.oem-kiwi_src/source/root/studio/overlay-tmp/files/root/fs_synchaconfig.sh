


<!DOCTYPE html>
<html>
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# githubog: http://ogp.me/ns/fb/githubog#">
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SAMBHA/overlay/hot/root/fs_synchaconfig.sh at master · juddy/SAMBHA · GitHub</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub" />
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    
    

    <meta content="authenticity_token" name="csrf-param" />
<meta content="u+a9oWr1MXjjLjbOVCvtvffHry+7/iHn+horDDF2e+g=" name="csrf-token" />

    <link href="https://a248.e.akamai.net/assets.github.com/stylesheets/bundles/github-f9e9af6c222638865ece5a631cda303e46d2ffd1.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="https://a248.e.akamai.net/assets.github.com/stylesheets/bundles/github2-c1921b914a1a5c9fd63f1f197c5742bcc5500740.css" media="screen" rel="stylesheet" type="text/css" />
    
    
    

    <script src="https://a248.e.akamai.net/assets.github.com/javascripts/bundles/frameworks-31b6b84bca1e7d3f3907f63a4dd7f9bbda3a0eda.js" type="text/javascript"></script>
    
    <script defer="defer" src="https://a248.e.akamai.net/assets.github.com/javascripts/bundles/github-f50998523925634c10e6ca6a1dc93b4ce27359ed.js" type="text/javascript"></script>
    
    

      <link rel='permalink' href='/juddy/SAMBHA/blob/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb/overlay/hot/root/fs_synchaconfig.sh'>
    <meta property="og:title" content="SAMBHA"/>
    <meta property="og:type" content="githubog:gitrepository"/>
    <meta property="og:url" content="https://github.com/juddy/SAMBHA"/>
    <meta property="og:image" content="https://a248.e.akamai.net/assets.github.com/images/gravatars/gravatar-140.png?1329920549"/>
    <meta property="og:site_name" content="GitHub"/>
    <meta property="og:description" content="DRBD + SAMBA + LinuxHA = SAMBHA"/>

    <meta name="description" content="DRBD + SAMBA + LinuxHA = SAMBHA" />

  <link href="https://github.com/juddy/SAMBHA/commits/master.atom" rel="alternate" title="Recent Commits to SAMBHA:master" type="application/atom+xml" />

  </head>


  <body class="logged_out page-blob  vis-public env-production " data-blob-contribs-enabled="yes">
    <div id="wrapper">

    
    
    

      <div id="header" class="true clearfix">
        <div class="container clearfix">
          <a class="site-logo" href="https://github.com">
            <!--[if IE]>
            <img alt="GitHub" class="github-logo" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7.png?1329920549" />
            <img alt="GitHub" class="github-logo-hover" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7-hover.png?1329920549" />
            <![endif]-->
            <img alt="GitHub" class="github-logo-4x" height="30" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7@4x.png?1329920549" />
            <img alt="GitHub" class="github-logo-4x-hover" height="30" src="https://a248.e.akamai.net/assets.github.com/images/modules/header/logov7@4x-hover.png?1329920549" />
          </a>

                  <!--
      make sure to use fully qualified URLs here since this nav
      is used on error pages on other domains
    -->
    <ul class="top-nav logged_out">
        <li class="pricing"><a href="https://github.com/plans">Signup and Pricing</a></li>
        <li class="explore"><a href="https://github.com/explore">Explore GitHub</a></li>
      <li class="features"><a href="https://github.com/features">Features</a></li>
        <li class="blog"><a href="https://github.com/blog">Blog</a></li>
      <li class="login"><a href="https://github.com/login?return_to=%2Fjuddy%2FSAMBHA%2Fblob%2Fmaster%2Foverlay%2Fhot%2Froot%2Ffs_synchaconfig.sh">Login</a></li>
    </ul>



          
        </div>
      </div>

      

            <div class="site hfeed" itemscope itemtype="http://schema.org/WebPage">
      <div class="container hentry">
        <div class="pagehead repohead instapaper_ignore readability-menu">
        <div class="title-actions-bar">
          



              <ul class="pagehead-actions">



          <li>
            <a href="/login?return_to=%2Fjuddy%2FSAMBHA" class="minibutton btn-i-type-i-switcher switcher count btn-watches js-toggler-target watch-button entice tooltipped leftwards" title="You must be logged in to use this feature" rel="nofollow"><span><span class="icon"></span><i>1</i> Watch</span></a>
          </li>
          <li>
            <a href="/login?return_to=%2Fjuddy%2FSAMBHA" class="minibutton btn-i-type-i-switcher switcher count btn-forks js-toggler-target fork-button entice tooltipped leftwards"  title="You must be logged in to use this feature" rel="nofollow"><span><span class="icon"></span><i>1</i> Fork</span></a>
          </li>

    </ul>

          <h1 itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="entry-title">
            <span class="repo-label"><span class="public">public</span></span>
            <span class="mega-icon public-repo"></span>
            <span class="author vcard">
<a href="/juddy" class="url fn" itemprop="url" rel="author">              <span itemprop="title">juddy</span>
              </a></span> /
            <strong><a href="/juddy/SAMBHA" class="js-current-repository">SAMBHA</a></strong>
          </h1>
        </div>

          

  <ul class="tabs">
    <li><a href="/juddy/SAMBHA" class="selected" highlight="repo_sourcerepo_downloadsrepo_commitsrepo_tagsrepo_branches">Code</a></li>
    <li><a href="/juddy/SAMBHA/network" highlight="repo_network">Network</a>
    <li><a href="/juddy/SAMBHA/pulls" highlight="repo_pulls">Pull Requests <span class='counter'>0</span></a></li>

      <li><a href="/juddy/SAMBHA/issues" highlight="repo_issues">Issues <span class='counter'>0</span></a></li>


    <li><a href="/juddy/SAMBHA/graphs" highlight="repo_graphsrepo_contributors">Graphs</a></li>

  </ul>
 
<div class="frame frame-center tree-finder" style="display:none"
      data-tree-list-url="/juddy/SAMBHA/tree-list/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb"
      data-blob-url-prefix="/juddy/SAMBHA/blob/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb"
    >

  <div class="breadcrumb">
    <span class="bold"><a href="/juddy/SAMBHA">SAMBHA</a></span> /
    <input class="tree-finder-input js-navigation-enable" type="text" name="query" autocomplete="off" spellcheck="false">
  </div>

    <div class="octotip">
      <p>
        <a href="/juddy/SAMBHA/dismiss-tree-finder-help" class="dismiss js-dismiss-tree-list-help" title="Hide this notice forever" rel="nofollow">Dismiss</a>
        <span class="bold">Octotip:</span> You've activated the <em>file finder</em>
        by pressing <span class="kbd">t</span> Start typing to filter the
        file list. Use <span class="kbd badmono">↑</span> and
        <span class="kbd badmono">↓</span> to navigate,
        <span class="kbd">enter</span> to view files.
      </p>
    </div>

  <table class="tree-browser" cellpadding="0" cellspacing="0">
    <tr class="js-header"><th>&nbsp;</th><th>name</th></tr>
    <tr class="js-no-results no-results" style="display: none">
      <th colspan="2">No matching files</th>
    </tr>
    <tbody class="js-results-list js-navigation-container">
    </tbody>
  </table>
</div>

<div id="jump-to-line" style="display:none">
  <h2>Jump to Line</h2>
  <form accept-charset="UTF-8">
    <input class="textfield" type="text">
    <div class="full-button">
      <button type="submit" class="classy">
        <span>Go</span>
      </button>
    </div>
  </form>
</div>


<div class="subnav-bar">

  <ul class="actions subnav">
    <li><a href="/juddy/SAMBHA/tags" class="blank" highlight="repo_tags">Tags <span class="counter">0</span></a></li>
    <li><a href="/juddy/SAMBHA/downloads" class="blank downloads-blank" highlight="repo_downloads">Downloads <span class="counter">0</span></a></li>
    
  </ul>

  <ul class="scope">
    <li class="switcher">

      <div class="context-menu-container js-menu-container js-context-menu">
        <a href="#"
           class="minibutton bigger switcher js-menu-target js-commitish-button btn-branch repo-tree"
           data-hotkey="w"
           data-master-branch="master"
           data-ref="master">
          <span><span class="icon"></span><i>branch:</i> master</span>
        </a>

        <div class="context-pane commitish-context js-menu-content">
          <a href="javascript:;" class="close js-menu-close"><span class="mini-icon remove-close"></span></a>
          <div class="context-title">Switch Branches/Tags</div>
          <div class="context-body pane-selector commitish-selector js-navigation-container">
            <div class="filterbar">
              <input type="text" id="context-commitish-filter-field" class="js-navigation-enable" placeholder="Filter branches/tags" data-filterable />

              <ul class="tabs">
                <li><a href="#" data-filter="branches" class="selected">Branches</a></li>
                <li><a href="#" data-filter="tags">Tags</a></li>
              </ul>
            </div>

            <div class="js-filter-tab js-filter-branches" data-filterable-for="context-commitish-filter-field">
              <div class="no-results js-not-filterable">Nothing to show</div>
                <div class="commitish-item branch-commitish selector-item js-navigation-item js-navigation-target">
                  <h4>
                      <a href="/juddy/SAMBHA/blob/master/overlay/hot/root/fs_synchaconfig.sh" class="js-navigation-open" data-name="master" rel="nofollow">master</a>
                  </h4>
                </div>
            </div>

            <div class="js-filter-tab js-filter-tags" style="display:none" data-filterable-for="context-commitish-filter-field">
              <div class="no-results js-not-filterable">Nothing to show</div>
            </div>
          </div>
        </div><!-- /.commitish-context-context -->
      </div>

    </li>
  </ul>

  <ul class="subnav with-scope">

    <li><a href="/juddy/SAMBHA" class="selected" highlight="repo_source">Files</a></li>
    <li><a href="/juddy/SAMBHA/commits/master" highlight="repo_commits">Commits</a></li>
    <li><a href="/juddy/SAMBHA/branches" class="" highlight="repo_branches" rel="nofollow">Branches <span class="counter">1</span></a></li>
  </ul>

</div>

  
  
  


          

        </div><!-- /.repohead -->

        





<!-- block_view_fragment_key: views8/v8/blob:v21:32c25f921f527c1abbaf61f2b22c615a -->
  <div id="slider">

    <div class="breadcrumb" data-path="overlay/hot/root/fs_synchaconfig.sh/">
      <b itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/juddy/SAMBHA/tree/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb" class="js-rewrite-sha" itemprop="url"><span itemprop="title">SAMBHA</span></a></b> / <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/juddy/SAMBHA/tree/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb/overlay" class="js-rewrite-sha" itemscope="url"><span itemprop="title">overlay</span></a></span> / <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/juddy/SAMBHA/tree/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb/overlay/hot" class="js-rewrite-sha" itemscope="url"><span itemprop="title">hot</span></a></span> / <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a href="/juddy/SAMBHA/tree/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb/overlay/hot/root" class="js-rewrite-sha" itemscope="url"><span itemprop="title">root</span></a></span> / <strong class="final-path">fs_synchaconfig.sh</strong> <span class="js-clippy mini-icon clippy " data-clipboard-text="overlay/hot/root/fs_synchaconfig.sh" data-copied-hint="copied!" data-copy-hint="copy to clipboard"></span>
    </div>


      <div class="commit file-history-tease" data-path="overlay/hot/root/fs_synchaconfig.sh/">
        <img class="main-avatar" height="24" src="https://secure.gravatar.com/avatar/1834bb2509df016abb34651bfe3b5bdf?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-140.png" width="24" />
        <span class="author"><a href="/juddy">juddy</a></span>
        <time class="js-relative-date" datetime="2012-05-13T11:19:26-07:00" title="2012-05-13 11:19:26">May 13, 2012</time>
        <div class="commit-title">
            <a href="/juddy/SAMBHA/commit/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb" class="message">added most recent root scripts</a>
        </div>

        <div class="participation">
          <p class="quickstat"><a href="#blob_contributors_box" rel="facebox"><strong>1</strong> contributor</a></p>
          
        </div>
        <div id="blob_contributors_box" style="display:none">
          <h2>Users on GitHub who have contributed to this file</h2>
          <ul class="facebox-user-list">
            <li>
              <img height="24" src="https://secure.gravatar.com/avatar/1834bb2509df016abb34651bfe3b5bdf?s=140&amp;d=https://a248.e.akamai.net/assets.github.com%2Fimages%2Fgravatars%2Fgravatar-140.png" width="24" />
              <a href="/juddy">juddy</a>
            </li>
          </ul>
        </div>
      </div>

    <div class="frames">
      <div class="frame frame-center" data-path="overlay/hot/root/fs_synchaconfig.sh/" data-permalink-url="/juddy/SAMBHA/blob/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb/overlay/hot/root/fs_synchaconfig.sh" data-title="SAMBHA/overlay/hot/root/fs_synchaconfig.sh at master · juddy/SAMBHA · GitHub" data-type="blob">

        <div id="files" class="bubble">
          <div class="file">
            <div class="meta">
              <div class="info">
                <span class="icon"><b class="mini-icon text-file"></b></span>
                <span class="mode" title="File Mode">100644</span>
                  <span>179 lines (135 sloc)</span>
                <span>4.23 kb</span>
              </div>
              <ul class="button-group actions">
                  <li>
                    <a class="grouped-button file-edit-link minibutton bigger lighter js-rewrite-sha" href="/juddy/SAMBHA/edit/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb/overlay/hot/root/fs_synchaconfig.sh" data-method="post" rel="nofollow" data-hotkey="e"><span>Edit this file</span></a>
                  </li>

                <li>
                  <a href="/juddy/SAMBHA/raw/master/overlay/hot/root/fs_synchaconfig.sh" class="minibutton btn-raw grouped-button bigger lighter" id="raw-url"><span><span class="icon"></span>Raw</span></a>
                </li>
                  <li>
                    <a href="/juddy/SAMBHA/blame/master/overlay/hot/root/fs_synchaconfig.sh" class="minibutton btn-blame grouped-button bigger lighter"><span><span class="icon"></span>Blame</span></a>
                  </li>
                <li>
                  <a href="/juddy/SAMBHA/commits/master/overlay/hot/root/fs_synchaconfig.sh" class="minibutton btn-history grouped-button bigger lighter" rel="nofollow"><span><span class="icon"></span>History</span></a>
                </li>
              </ul>
            </div>
              <div class="data type-shell">
      <table cellpadding="0" cellspacing="0" class="lines">
        <tr>
          <td>
            <pre class="line_numbers"><span id="L1" rel="#L1">1</span>
<span id="L2" rel="#L2">2</span>
<span id="L3" rel="#L3">3</span>
<span id="L4" rel="#L4">4</span>
<span id="L5" rel="#L5">5</span>
<span id="L6" rel="#L6">6</span>
<span id="L7" rel="#L7">7</span>
<span id="L8" rel="#L8">8</span>
<span id="L9" rel="#L9">9</span>
<span id="L10" rel="#L10">10</span>
<span id="L11" rel="#L11">11</span>
<span id="L12" rel="#L12">12</span>
<span id="L13" rel="#L13">13</span>
<span id="L14" rel="#L14">14</span>
<span id="L15" rel="#L15">15</span>
<span id="L16" rel="#L16">16</span>
<span id="L17" rel="#L17">17</span>
<span id="L18" rel="#L18">18</span>
<span id="L19" rel="#L19">19</span>
<span id="L20" rel="#L20">20</span>
<span id="L21" rel="#L21">21</span>
<span id="L22" rel="#L22">22</span>
<span id="L23" rel="#L23">23</span>
<span id="L24" rel="#L24">24</span>
<span id="L25" rel="#L25">25</span>
<span id="L26" rel="#L26">26</span>
<span id="L27" rel="#L27">27</span>
<span id="L28" rel="#L28">28</span>
<span id="L29" rel="#L29">29</span>
<span id="L30" rel="#L30">30</span>
<span id="L31" rel="#L31">31</span>
<span id="L32" rel="#L32">32</span>
<span id="L33" rel="#L33">33</span>
<span id="L34" rel="#L34">34</span>
<span id="L35" rel="#L35">35</span>
<span id="L36" rel="#L36">36</span>
<span id="L37" rel="#L37">37</span>
<span id="L38" rel="#L38">38</span>
<span id="L39" rel="#L39">39</span>
<span id="L40" rel="#L40">40</span>
<span id="L41" rel="#L41">41</span>
<span id="L42" rel="#L42">42</span>
<span id="L43" rel="#L43">43</span>
<span id="L44" rel="#L44">44</span>
<span id="L45" rel="#L45">45</span>
<span id="L46" rel="#L46">46</span>
<span id="L47" rel="#L47">47</span>
<span id="L48" rel="#L48">48</span>
<span id="L49" rel="#L49">49</span>
<span id="L50" rel="#L50">50</span>
<span id="L51" rel="#L51">51</span>
<span id="L52" rel="#L52">52</span>
<span id="L53" rel="#L53">53</span>
<span id="L54" rel="#L54">54</span>
<span id="L55" rel="#L55">55</span>
<span id="L56" rel="#L56">56</span>
<span id="L57" rel="#L57">57</span>
<span id="L58" rel="#L58">58</span>
<span id="L59" rel="#L59">59</span>
<span id="L60" rel="#L60">60</span>
<span id="L61" rel="#L61">61</span>
<span id="L62" rel="#L62">62</span>
<span id="L63" rel="#L63">63</span>
<span id="L64" rel="#L64">64</span>
<span id="L65" rel="#L65">65</span>
<span id="L66" rel="#L66">66</span>
<span id="L67" rel="#L67">67</span>
<span id="L68" rel="#L68">68</span>
<span id="L69" rel="#L69">69</span>
<span id="L70" rel="#L70">70</span>
<span id="L71" rel="#L71">71</span>
<span id="L72" rel="#L72">72</span>
<span id="L73" rel="#L73">73</span>
<span id="L74" rel="#L74">74</span>
<span id="L75" rel="#L75">75</span>
<span id="L76" rel="#L76">76</span>
<span id="L77" rel="#L77">77</span>
<span id="L78" rel="#L78">78</span>
<span id="L79" rel="#L79">79</span>
<span id="L80" rel="#L80">80</span>
<span id="L81" rel="#L81">81</span>
<span id="L82" rel="#L82">82</span>
<span id="L83" rel="#L83">83</span>
<span id="L84" rel="#L84">84</span>
<span id="L85" rel="#L85">85</span>
<span id="L86" rel="#L86">86</span>
<span id="L87" rel="#L87">87</span>
<span id="L88" rel="#L88">88</span>
<span id="L89" rel="#L89">89</span>
<span id="L90" rel="#L90">90</span>
<span id="L91" rel="#L91">91</span>
<span id="L92" rel="#L92">92</span>
<span id="L93" rel="#L93">93</span>
<span id="L94" rel="#L94">94</span>
<span id="L95" rel="#L95">95</span>
<span id="L96" rel="#L96">96</span>
<span id="L97" rel="#L97">97</span>
<span id="L98" rel="#L98">98</span>
<span id="L99" rel="#L99">99</span>
<span id="L100" rel="#L100">100</span>
<span id="L101" rel="#L101">101</span>
<span id="L102" rel="#L102">102</span>
<span id="L103" rel="#L103">103</span>
<span id="L104" rel="#L104">104</span>
<span id="L105" rel="#L105">105</span>
<span id="L106" rel="#L106">106</span>
<span id="L107" rel="#L107">107</span>
<span id="L108" rel="#L108">108</span>
<span id="L109" rel="#L109">109</span>
<span id="L110" rel="#L110">110</span>
<span id="L111" rel="#L111">111</span>
<span id="L112" rel="#L112">112</span>
<span id="L113" rel="#L113">113</span>
<span id="L114" rel="#L114">114</span>
<span id="L115" rel="#L115">115</span>
<span id="L116" rel="#L116">116</span>
<span id="L117" rel="#L117">117</span>
<span id="L118" rel="#L118">118</span>
<span id="L119" rel="#L119">119</span>
<span id="L120" rel="#L120">120</span>
<span id="L121" rel="#L121">121</span>
<span id="L122" rel="#L122">122</span>
<span id="L123" rel="#L123">123</span>
<span id="L124" rel="#L124">124</span>
<span id="L125" rel="#L125">125</span>
<span id="L126" rel="#L126">126</span>
<span id="L127" rel="#L127">127</span>
<span id="L128" rel="#L128">128</span>
<span id="L129" rel="#L129">129</span>
<span id="L130" rel="#L130">130</span>
<span id="L131" rel="#L131">131</span>
<span id="L132" rel="#L132">132</span>
<span id="L133" rel="#L133">133</span>
<span id="L134" rel="#L134">134</span>
<span id="L135" rel="#L135">135</span>
<span id="L136" rel="#L136">136</span>
<span id="L137" rel="#L137">137</span>
<span id="L138" rel="#L138">138</span>
<span id="L139" rel="#L139">139</span>
<span id="L140" rel="#L140">140</span>
<span id="L141" rel="#L141">141</span>
<span id="L142" rel="#L142">142</span>
<span id="L143" rel="#L143">143</span>
<span id="L144" rel="#L144">144</span>
<span id="L145" rel="#L145">145</span>
<span id="L146" rel="#L146">146</span>
<span id="L147" rel="#L147">147</span>
<span id="L148" rel="#L148">148</span>
<span id="L149" rel="#L149">149</span>
<span id="L150" rel="#L150">150</span>
<span id="L151" rel="#L151">151</span>
<span id="L152" rel="#L152">152</span>
<span id="L153" rel="#L153">153</span>
<span id="L154" rel="#L154">154</span>
<span id="L155" rel="#L155">155</span>
<span id="L156" rel="#L156">156</span>
<span id="L157" rel="#L157">157</span>
<span id="L158" rel="#L158">158</span>
<span id="L159" rel="#L159">159</span>
<span id="L160" rel="#L160">160</span>
<span id="L161" rel="#L161">161</span>
<span id="L162" rel="#L162">162</span>
<span id="L163" rel="#L163">163</span>
<span id="L164" rel="#L164">164</span>
<span id="L165" rel="#L165">165</span>
<span id="L166" rel="#L166">166</span>
<span id="L167" rel="#L167">167</span>
<span id="L168" rel="#L168">168</span>
<span id="L169" rel="#L169">169</span>
<span id="L170" rel="#L170">170</span>
<span id="L171" rel="#L171">171</span>
<span id="L172" rel="#L172">172</span>
<span id="L173" rel="#L173">173</span>
<span id="L174" rel="#L174">174</span>
<span id="L175" rel="#L175">175</span>
<span id="L176" rel="#L176">176</span>
<span id="L177" rel="#L177">177</span>
<span id="L178" rel="#L178">178</span>
</pre>
          </td>
          <td width="100%">
                <div class="highlight"><pre><div class='line' id='LC1'><span class="c">#!/bin/bash</span></div><div class='line' id='LC2'><br/></div><div class='line' id='LC3'><span class="c"># Find out where we are</span></div><div class='line' id='LC4'><span class="nv">HOSTNAME</span><span class="o">=</span><span class="k">$(</span>uname -n<span class="k">)</span></div><div class='line' id='LC5'><br/></div><div class='line' id='LC6'><span class="k">case</span> <span class="nv">$HOSTNAME</span> in</div><div class='line' id='LC7'><br/></div><div class='line' id='LC8'>	hot<span class="o">)</span></div><div class='line' id='LC9'>		<span class="nb">echo</span> <span class="s2">&quot;I am the Hot server..&quot;</span></div><div class='line' id='LC10'>		<span class="nv">MYHOSTNAME</span><span class="o">=</span><span class="s2">&quot;hot&quot;</span></div><div class='line' id='LC11'>		<span class="nv">OTHERHOSTNAME</span><span class="o">=</span><span class="s2">&quot;warm&quot;</span></div><div class='line' id='LC12'>	;;</div><div class='line' id='LC13'><br/></div><div class='line' id='LC14'>	warm<span class="o">)</span></div><div class='line' id='LC15'>		<span class="nb">echo</span> <span class="s2">&quot;I am the Warm server..&quot;</span></div><div class='line' id='LC16'>		<span class="nv">MYHOSTNAME</span><span class="o">=</span><span class="s2">&quot;warm&quot;</span></div><div class='line' id='LC17'>		<span class="nv">OTHERHOSTNAME</span><span class="o">=</span><span class="s2">&quot;hot&quot;</span></div><div class='line' id='LC18'>	;;</div><div class='line' id='LC19'><br/></div><div class='line' id='LC20'>	*<span class="o">)</span></div><div class='line' id='LC21'>		<span class="nb">echo</span> <span class="s2">&quot;FATAL: Failed to get hostname.&quot;</span></div><div class='line' id='LC22'>		<span class="nb">exit </span>1</div><div class='line' id='LC23'>	;;</div><div class='line' id='LC24'><span class="k">esac</span></div><div class='line' id='LC25'><br/></div><div class='line' id='LC26'><br/></div><div class='line' id='LC27'><br/></div><div class='line' id='LC28'><br/></div><div class='line' id='LC29'><span class="c"># If we can&#39;t perform the rsync operation, error out</span></div><div class='line' id='LC30'>check_result<span class="o">(){</span></div><div class='line' id='LC31'><br/></div><div class='line' id='LC32'><span class="k">if</span> <span class="o">[</span> <span class="nv">$?</span> -ne 0 <span class="o">]</span></div><div class='line' id='LC33'><span class="k">then</span></div><div class='line' id='LC34'><span class="k">	</span><span class="nb">echo</span> <span class="s2">&quot;Error synchronizing directory.  Check connection to $OTHERHOSTNAME.&quot;</span></div><div class='line' id='LC35'>	<span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span></div><div class='line' id='LC36'>	<span class="nb">echo</span> <span class="s2">&quot;/etc/hosts says:&quot;</span></div><div class='line' id='LC37'>	grep <span class="nv">$OTHERHOSTNAME</span> /etc/hosts</div><div class='line' id='LC38'>	<span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span></div><div class='line' id='LC39'><span class="k">fi</span></div><div class='line' id='LC40'><br/></div><div class='line' id='LC41'><span class="o">}</span></div><div class='line' id='LC42'><br/></div><div class='line' id='LC43'><br/></div><div class='line' id='LC44'><br/></div><div class='line' id='LC45'><br/></div><div class='line' id='LC46'><span class="nb">echo</span> <span class="s2">&quot;Syncing up HA related directories and files to $OTHERHOSTNAME&quot;</span></div><div class='line' id='LC47'><span class="c"># This copies everything in this ha.d directory</span></div><div class='line' id='LC48'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span></div><div class='line' id='LC49'><span class="nb">echo</span> <span class="s2">&quot;ha.d..&quot;</span></div><div class='line' id='LC50'>rsync -av /etc/ha.d/ root@<span class="nv">$OTHERHOSTNAME</span>:/etc/ha.d/ --backup --suffix<span class="o">=</span>.<span class="k">$(</span>date +%Y%m%sd<span class="k">)</span>.bak</div><div class='line' id='LC51'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span> ; check_result</div><div class='line' id='LC52'><br/></div><div class='line' id='LC53'><br/></div><div class='line' id='LC54'><span class="nb">echo</span> <span class="s2">&quot;Syncing up SAMBA related directories and files..&quot;</span></div><div class='line' id='LC55'><span class="c"># This copies everything in this /etc/samba directory</span></div><div class='line' id='LC56'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span></div><div class='line' id='LC57'><span class="nb">echo</span> <span class="s2">&quot;samba..&quot;</span></div><div class='line' id='LC58'>rsync -av /etc/samba/ root@<span class="nv">$OTHERHOSTNAME</span>:/etc/samba/ --backup --suffix<span class="o">=</span><span class="k">$(</span>date +%Y%m%sd<span class="k">)</span>.bak</div><div class='line' id='LC59'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span> ; check_result</div><div class='line' id='LC60'><br/></div><div class='line' id='LC61'><span class="nb">echo</span> <span class="s2">&quot;Syncing up DRBD related directories and files..&quot;</span></div><div class='line' id='LC62'><span class="c"># This copies everything in this /etc/drbd directory</span></div><div class='line' id='LC63'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span></div><div class='line' id='LC64'><span class="nb">echo</span> <span class="s2">&quot;drbd..&quot;</span></div><div class='line' id='LC65'>rsync -av /etc/drbd* root@<span class="nv">$OTHERHOSTNAME</span>:/etc/ --backup --suffix<span class="o">=</span><span class="k">$(</span>date +%Y%m%sd<span class="k">)</span>.bak</div><div class='line' id='LC66'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span> ; check_result</div><div class='line' id='LC67'><br/></div><div class='line' id='LC68'><span class="nb">echo</span> <span class="s2">&quot;Syncing up Admin scripts..&quot;</span></div><div class='line' id='LC69'><span class="c"># This copies all the shell scripts in /root/&quot;</span></div><div class='line' id='LC70'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span></div><div class='line' id='LC71'><span class="nb">echo</span> <span class="s2">&quot;scripts..&quot;</span></div><div class='line' id='LC72'>rsync -av /root/*.sh root@<span class="nv">$OTHERHOSTNAME</span>:/root/ --backup --suffix<span class="o">=</span><span class="k">$(</span>date +%Y%m%sd<span class="k">)</span>.bak</div><div class='line' id='LC73'><span class="nb">echo</span> <span class="s2">&quot;-------------------&quot;</span> ; check_result</div><div class='line' id='LC74'><br/></div><div class='line' id='LC75'><br/></div><div class='line' id='LC76'><span class="nb">echo</span> ; <span class="nb">echo</span> <span class="s2">&quot;Restart heartbeat and DRBD?&quot;</span></div><div class='line' id='LC77'><span class="nb">echo</span> <span class="s2">&quot;Default is &#39;no&#39;&quot;</span></div><div class='line' id='LC78'><span class="nb">echo</span> ; <span class="nb">echo</span> <span class="s2">&quot;[y|N]&quot;</span></div><div class='line' id='LC79'><br/></div><div class='line' id='LC80'><span class="nb">read </span>hbrestart</div><div class='line' id='LC81'><br/></div><div class='line' id='LC82'><span class="k">case</span> <span class="nv">$hbrestart</span> in</div><div class='line' id='LC83'><br/></div><div class='line' id='LC84'>	n|N<span class="o">)</span></div><div class='line' id='LC85'>		<span class="nb">echo</span> <span class="s2">&quot;To restart heartbeat, issue /etc/init.d/heartbeat restart on warm and hot hosts.&quot;</span></div><div class='line' id='LC86'>		<span class="nb">echo</span> <span class="s2">&quot;To restart DRBD, issue /etc/init.d/drbd restart on warm and hot hosts.&quot;</span></div><div class='line' id='LC87'>	;;</div><div class='line' id='LC88'><br/></div><div class='line' id='LC89'>	y|Y<span class="o">)</span></div><div class='line' id='LC90'>		<span class="nb">echo</span> <span class="s2">&quot;Restarting heartbeat on warm and hot hosts...&quot;</span></div><div class='line' id='LC91'>		/etc/init.d/heartbeat stop</div><div class='line' id='LC92'>		/etc/init.d/heartbeat start</div><div class='line' id='LC93'><br/></div><div class='line' id='LC94'>		<span class="k">if</span> <span class="o">[</span> <span class="nv">$?</span> -ne 0 <span class="o">]</span></div><div class='line' id='LC95'>		<span class="k">then</span></div><div class='line' id='LC96'><span class="k">			</span><span class="nb">echo</span> <span class="s2">&quot;Failed to restart heartbeat.  Investigate and restart manually.&quot;</span></div><div class='line' id='LC97'>		<span class="k">else</span></div><div class='line' id='LC98'><span class="k">			</span><span class="nb">echo</span> <span class="s2">&quot;Stopping heartbeat on $OTHERHOSTNAME&quot;</span></div><div class='line' id='LC99'>			ssh <span class="nv">$OTHERHOSTNAME</span> /etc/init.d/heartbeat stop</div><div class='line' id='LC100'>			<span class="k">if</span> <span class="o">[</span> <span class="nv">$?</span> -ne 0 <span class="o">]</span></div><div class='line' id='LC101'>			<span class="k">then</span></div><div class='line' id='LC102'><span class="k">				</span><span class="nb">echo</span> <span class="s2">&quot;Failed to restart heartbeat.  Investigate and restart manually.&quot;</span></div><div class='line' id='LC103'>			<span class="k">else</span></div><div class='line' id='LC104'><span class="k">				</span><span class="nb">echo</span> <span class="s2">&quot;watch..&quot;</span></div><div class='line' id='LC105'>			<span class="k">fi</span></div><div class='line' id='LC106'><span class="k">				</span></div><div class='line' id='LC107'><span class="k">		</span></div><div class='line' id='LC108'><span class="k">		fi</span></div><div class='line' id='LC109'><br/></div><div class='line' id='LC110'><span class="k">		</span><span class="nb">echo</span> <span class="s2">&quot;Restarting DRBD on warm and hot hosts...&quot;</span></div><div class='line' id='LC111'>		<span class="nb">echo</span> <span class="s2">&quot;Stopping DRBD locally..&quot;</span></div><div class='line' id='LC112'>		/etc/init.d/drbd stop</div><div class='line' id='LC113'><br/></div><div class='line' id='LC114'>		<span class="nb">echo</span> <span class="s2">&quot;Starting DRBD on localhost..&quot;</span></div><div class='line' id='LC115'>		/etc/init.d/drbd start</div><div class='line' id='LC116'><br/></div><div class='line' id='LC117'>		<span class="k">if</span> <span class="o">[</span> <span class="nv">$?</span> -ne 0 <span class="o">]</span></div><div class='line' id='LC118'>		<span class="k">then</span></div><div class='line' id='LC119'><span class="k">			</span><span class="nb">echo</span> <span class="s2">&quot;Failed to restart drbd.  Investigate and restart manually.&quot;</span></div><div class='line' id='LC120'>			<span class="nb">exit </span>1</div><div class='line' id='LC121'>		<span class="k">else</span></div><div class='line' id='LC122'><span class="k">			</span><span class="nb">echo</span> <span class="s2">&quot;Stopping drbd on $OTHERHOSTNAME&quot;</span></div><div class='line' id='LC123'>			ssh <span class="nv">$OTHERHOSTNAME</span> /etc/init.d/drbd stop</div><div class='line' id='LC124'>			<span class="k">if</span> <span class="o">[</span> <span class="nv">$?</span> -ne 0 <span class="o">]</span></div><div class='line' id='LC125'>			<span class="k">then</span></div><div class='line' id='LC126'><span class="k">				</span><span class="nb">echo</span> <span class="s2">&quot;Failed to stop drbd on $OTHERHOSTNAME.  Investigate and restart manually.&quot;</span></div><div class='line' id='LC127'>			<span class="k">else</span></div><div class='line' id='LC128'><span class="k">				</span><span class="nb">echo</span> <span class="s2">&quot;Starting drbd on $OTHERHOSTNAME&quot;</span></div><div class='line' id='LC129'>				ssh <span class="nv">$OTHERHOSTNAME</span> /etc/init.d/drbd start</div><div class='line' id='LC130'>			<span class="k">fi</span></div><div class='line' id='LC131'><span class="k">				</span></div><div class='line' id='LC132'><span class="k">		</span></div><div class='line' id='LC133'><span class="k">		fi</span></div><div class='line' id='LC134'><br/></div><div class='line' id='LC135'>		<span class="c"># Restart heartbeat here and there</span></div><div class='line' id='LC136'>		<span class="nb">echo</span> <span class="s2">&quot;Starting heartbeat on $OTHERHOSTNAME&quot;</span></div><div class='line' id='LC137'>		ssh <span class="nv">$OTHERHOSTNAME</span> /etc/init.d/heartbeat start</div><div class='line' id='LC138'>		<span class="nb">echo</span> <span class="s2">&quot;starting heartbeat locally..&quot;</span></div><div class='line' id='LC139'>		/etc/init.d/heartbeat start</div><div class='line' id='LC140'>	;;</div><div class='line' id='LC141'><br/></div><div class='line' id='LC142'>	*<span class="o">)</span></div><div class='line' id='LC143'>		<span class="nb">echo</span> <span class="s2">&quot;To restart heartbeat, issue /etc/init.d/heartbeat restart on warm and hot hosts.&quot;</span></div><div class='line' id='LC144'>		<span class="nb">echo</span> <span class="s2">&quot;To restart DRBD, issue /etc/init.d/drbd restart on warm and hot hosts.&quot;</span></div><div class='line' id='LC145'>	;;</div><div class='line' id='LC146'><br/></div><div class='line' id='LC147'><span class="k">esac</span> </div><div class='line' id='LC148'><br/></div><div class='line' id='LC149'><span class="c"># Check DRBD status</span></div><div class='line' id='LC150'><span class="nv">DRBDLOCALSTATUS</span><span class="o">=</span><span class="k">$(</span>/etc/init.d/drbd status | tail -n 1 | awk <span class="s1">&#39;{print $3}&#39;</span><span class="k">)</span></div><div class='line' id='LC151'><br/></div><div class='line' id='LC152'><span class="c"># Establish DRBD status</span></div><div class='line' id='LC153'><span class="k">case</span> <span class="nv">$DRBDLOCALSTATUS</span> in</div><div class='line' id='LC154'><br/></div><div class='line' id='LC155'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Secondary/Unknown<span class="o">)</span></div><div class='line' id='LC156'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="nv">DRBDSECONDARY</span><span class="o">=</span>1</div><div class='line' id='LC157'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssh <span class="nv">$OTHERHOSTNAME</span> drbdadm primary all</div><div class='line' id='LC158'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drbdadm disconnect all</div><div class='line' id='LC159'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;drbdadm -- --discard-my-data connect all</div><div class='line' id='LC160'>;;</div><div class='line' id='LC161'><br/></div><div class='line' id='LC162'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Primary/Unknown<span class="o">)</span></div><div class='line' id='LC163'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="nv">DRBDPRIMARY</span><span class="o">=</span>1</div><div class='line' id='LC164'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssh <span class="nv">$OTHERHOSTNAME</span> drbdadm disconnect all</div><div class='line' id='LC165'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssh <span class="nv">$OTHERHOSTNAME</span> drbdadm -- --discard-my-data connect all</div><div class='line' id='LC166'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;;;</div><div class='line' id='LC167'><br/></div><div class='line' id='LC168'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*<span class="o">)</span></div><div class='line' id='LC169'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="nb">echo</span> <span class="s2">&quot;Undesirable DRBD status discovered..&quot;</span></div><div class='line' id='LC170'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="nb">echo</span> <span class="s2">&quot;Attempting to force state.&quot;</span></div><div class='line' id='LC171'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="nb">echo</span></div><div class='line' id='LC172'><span class="nb">        </span>drbdadm primary all</div><div class='line' id='LC173'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ssh <span class="nv">$OTHERHOSTNAME</span> drbdadm secondary all</div><div class='line' id='LC174'><br/></div><div class='line' id='LC175'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;;;</div><div class='line' id='LC176'><span class="k">esac</span></div><div class='line' id='LC177'><br/></div><div class='line' id='LC178'>	/etc/init.d/drbd status</div></pre></div>
          </td>
        </tr>
      </table>
  </div>

          </div>
        </div>
      </div>
    </div>

  </div>

<div class="frame frame-loading large-loading-area" style="display:none;" data-tree-list-url="/juddy/SAMBHA/tree-list/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb" data-blob-url-prefix="/juddy/SAMBHA/blob/6ba943c1c5397af84b2c55c34905aa83e7a6f0bb">
  <img src="https://a248.e.akamai.net/assets.github.com/images/spinners/octocat-spinner-64.gif?1329920549" height="64" width="64">
</div>

      </div>
      <div class="context-overlay"></div>
    </div>

      <div id="footer-push"></div><!-- hack for sticky footer -->
    </div><!-- end of wrapper - hack for sticky footer -->

      <!-- footer -->
      <div id="footer" >
        
  <div class="upper_footer">
     <div class="container clearfix">

       <!--[if IE]><h4 id="blacktocat_ie">GitHub Links</h4><![endif]-->
       <![if !IE]><h4 id="blacktocat">GitHub Links</h4><![endif]>

       <ul class="footer_nav">
         <h4>GitHub</h4>
         <li><a href="https://github.com/about">About</a></li>
         <li><a href="https://github.com/blog">Blog</a></li>
         <li><a href="https://github.com/features">Features</a></li>
         <li><a href="https://github.com/contact">Contact &amp; Support</a></li>
         <li><a href="https://github.com/training">Training</a></li>
         <li><a href="http://enterprise.github.com/">GitHub Enterprise</a></li>
         <li><a href="http://status.github.com/">Site Status</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Tools</h4>
         <li><a href="http://get.gaug.es/">Gauges: Analyze web traffic</a></li>
         <li><a href="http://speakerdeck.com">Speaker Deck: Presentations</a></li>
         <li><a href="https://gist.github.com">Gist: Code snippets</a></li>
         <li><a href="http://mac.github.com/">GitHub for Mac</a></li>
         <li><a href="http://mobile.github.com/">Issues for iPhone</a></li>
         <li><a href="http://jobs.github.com/">Job Board</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Extras</h4>
         <li><a href="http://shop.github.com/">GitHub Shop</a></li>
         <li><a href="http://octodex.github.com/">The Octodex</a></li>
       </ul>

       <ul class="footer_nav">
         <h4>Documentation</h4>
         <li><a href="http://help.github.com/">GitHub Help</a></li>
         <li><a href="http://developer.github.com/">Developer API</a></li>
         <li><a href="http://github.github.com/github-flavored-markdown/">GitHub Flavored Markdown</a></li>
         <li><a href="http://pages.github.com/">GitHub Pages</a></li>
       </ul>

     </div><!-- /.site -->
  </div><!-- /.upper_footer -->

<div class="lower_footer">
  <div class="container clearfix">
    <!--[if IE]><div id="legal_ie"><![endif]-->
    <![if !IE]><div id="legal"><![endif]>
      <ul>
          <li><a href="https://github.com/site/terms">Terms of Service</a></li>
          <li><a href="https://github.com/site/privacy">Privacy</a></li>
          <li><a href="https://github.com/security">Security</a></li>
      </ul>

      <p>&copy; 2012 <span title="0.05010s from fe15.rs.github.com">GitHub</span> Inc. All rights reserved.</p>
    </div><!-- /#legal or /#legal_ie-->

      <div class="sponsor">
        <a href="http://www.rackspace.com" class="logo">
          <img alt="Dedicated Server" height="36" src="https://a248.e.akamai.net/assets.github.com/images/modules/footer/rackspaces_logo.png?1329920549" width="38" />
        </a>
        Powered by the <a href="http://www.rackspace.com ">Dedicated
        Servers</a> and<br/> <a href="http://www.rackspacecloud.com">Cloud
        Computing</a> of Rackspace Hosting<span>&reg;</span>
      </div>
  </div><!-- /.site -->
</div><!-- /.lower_footer -->

      </div><!-- /#footer -->

    

<div id="keyboard_shortcuts_pane" class="instapaper_ignore readability-extra" style="display:none">
  <h2>Keyboard Shortcuts <small><a href="#" class="js-see-all-keyboard-shortcuts">(see all)</a></small></h2>

  <div class="columns threecols">
    <div class="column first">
      <h3>Site wide shortcuts</h3>
      <dl class="keyboard-mappings">
        <dt>s</dt>
        <dd>Focus site search</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>?</dt>
        <dd>Bring up this help dialog</dd>
      </dl>
    </div><!-- /.column.first -->

    <div class="column middle" style='display:none'>
      <h3>Commit list</h3>
      <dl class="keyboard-mappings">
        <dt>j</dt>
        <dd>Move selection down</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>k</dt>
        <dd>Move selection up</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>c <em>or</em> o <em>or</em> enter</dt>
        <dd>Open commit</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>y</dt>
        <dd>Expand URL to its canonical form</dd>
      </dl>
    </div><!-- /.column.first -->

    <div class="column last" style='display:none'>
      <h3>Pull request list</h3>
      <dl class="keyboard-mappings">
        <dt>j</dt>
        <dd>Move selection down</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>k</dt>
        <dd>Move selection up</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt>o <em>or</em> enter</dt>
        <dd>Open issue</dd>
      </dl>
      <dl class="keyboard-mappings">
        <dt><span class="platform-mac">⌘</span><span class="platform-other">ctrl</span> <em>+</em> enter</dt>
        <dd>Submit comment</dd>
      </dl>
    </div><!-- /.columns.last -->

  </div><!-- /.columns.equacols -->

  <div style='display:none'>
    <div class="rule"></div>

    <h3>Issues</h3>

    <div class="columns threecols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt>j</dt>
          <dd>Move selection down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>k</dt>
          <dd>Move selection up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>x</dt>
          <dd>Toggle selection</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o <em>or</em> enter</dt>
          <dd>Open issue</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="platform-mac">⌘</span><span class="platform-other">ctrl</span> <em>+</em> enter</dt>
          <dd>Submit comment</dd>
        </dl>
      </div><!-- /.column.first -->
      <div class="column last">
        <dl class="keyboard-mappings">
          <dt>c</dt>
          <dd>Create issue</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>l</dt>
          <dd>Create label</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>i</dt>
          <dd>Back to inbox</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>u</dt>
          <dd>Back to issues</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>/</dt>
          <dd>Focus issues search</dd>
        </dl>
      </div>
    </div>
  </div>

  <div style='display:none'>
    <div class="rule"></div>

    <h3>Issues Dashboard</h3>

    <div class="columns threecols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt>j</dt>
          <dd>Move selection down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>k</dt>
          <dd>Move selection up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o <em>or</em> enter</dt>
          <dd>Open issue</dd>
        </dl>
      </div><!-- /.column.first -->
    </div>
  </div>

  <div style='display:none'>
    <div class="rule"></div>

    <h3>Network Graph</h3>
    <div class="columns equacols">
      <div class="column first">
        <dl class="keyboard-mappings">
          <dt><span class="badmono">←</span> <em>or</em> h</dt>
          <dd>Scroll left</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">→</span> <em>or</em> l</dt>
          <dd>Scroll right</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">↑</span> <em>or</em> k</dt>
          <dd>Scroll up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt><span class="badmono">↓</span> <em>or</em> j</dt>
          <dd>Scroll down</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>t</dt>
          <dd>Toggle visibility of head labels</dd>
        </dl>
      </div><!-- /.column.first -->
      <div class="column last">
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">←</span> <em>or</em> shift h</dt>
          <dd>Scroll all the way left</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">→</span> <em>or</em> shift l</dt>
          <dd>Scroll all the way right</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">↑</span> <em>or</em> shift k</dt>
          <dd>Scroll all the way up</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>shift <span class="badmono">↓</span> <em>or</em> shift j</dt>
          <dd>Scroll all the way down</dd>
        </dl>
      </div><!-- /.column.last -->
    </div>
  </div>

  <div >
    <div class="rule"></div>
    <div class="columns threecols">
      <div class="column first" >
        <h3>Source Code Browsing</h3>
        <dl class="keyboard-mappings">
          <dt>t</dt>
          <dd>Activates the file finder</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>l</dt>
          <dd>Jump to line</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>w</dt>
          <dd>Switch branch/tag</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>y</dt>
          <dd>Expand URL to its canonical form</dd>
        </dl>
      </div>
    </div>
  </div>

  <div style='display:none'>
    <div class="rule"></div>
    <div class="columns threecols">
      <div class="column first">
        <h3>Browsing Commits</h3>
        <dl class="keyboard-mappings">
          <dt><span class="platform-mac">⌘</span><span class="platform-other">ctrl</span> <em>+</em> enter</dt>
          <dd>Submit comment</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>escape</dt>
          <dd>Close form</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>p</dt>
          <dd>Parent commit</dd>
        </dl>
        <dl class="keyboard-mappings">
          <dt>o</dt>
          <dd>Other parent commit</dd>
        </dl>
      </div>
    </div>
  </div>
</div>

    <div id="markdown-help" class="instapaper_ignore readability-extra">
  <h2>Markdown Cheat Sheet</h2>

  <div class="cheatsheet-content">

  <div class="mod">
    <div class="col">
      <h3>Format Text</h3>
      <p>Headers</p>
      <pre>
# This is an &lt;h1&gt; tag
## This is an &lt;h2&gt; tag
###### This is an &lt;h6&gt; tag</pre>
     <p>Text styles</p>
     <pre>
*This text will be italic*
_This will also be italic_
**This text will be bold**
__This will also be bold__

*You **can** combine them*
</pre>
    </div>
    <div class="col">
      <h3>Lists</h3>
      <p>Unordered</p>
      <pre>
* Item 1
* Item 2
  * Item 2a
  * Item 2b</pre>
     <p>Ordered</p>
     <pre>
1. Item 1
2. Item 2
3. Item 3
   * Item 3a
   * Item 3b</pre>
    </div>
    <div class="col">
      <h3>Miscellaneous</h3>
      <p>Images</p>
      <pre>
![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)
</pre>
     <p>Links</p>
     <pre>
http://github.com - automatic!
[GitHub](http://github.com)</pre>
<p>Blockquotes</p>
     <pre>
As Kanye West said:

> We're living the future so
> the present is our past.
</pre>
    </div>
  </div>
  <div class="rule"></div>

  <h3>Code Examples in Markdown</h3>
  <div class="col">
      <p>Syntax highlighting with <a href="http://github.github.com/github-flavored-markdown/" title="GitHub Flavored Markdown" target="_blank">GFM</a></p>
      <pre>
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```</pre>
    </div>
    <div class="col">
      <p>Or, indent your code 4 spaces</p>
      <pre>
Here is a Python code example
without syntax highlighting:

    def foo:
      if not bar:
        return true</pre>
    </div>
    <div class="col">
      <p>Inline code for comments</p>
      <pre>
I think you should use an
`&lt;addr&gt;` element here instead.</pre>
    </div>
  </div>

  </div>
</div>


    <div class="ajax-error-message">
      <p><span class="mini-icon exclamation"></span> Something went wrong with that request. Please try again. <a href="javascript:;" class="ajax-error-dismiss">Dismiss</a></p>
    </div>

    <div id="logo-popup">
      <h2>Looking for the GitHub logo?</h2>
      <ul>
        <li>
          <h4>GitHub Logo</h4>
          <a href="http://github-media-downloads.s3.amazonaws.com/GitHub_Logos.zip"><img alt="Github_logo" src="https://a248.e.akamai.net/assets.github.com/images/modules/about_page/github_logo.png?1329920549" /></a>
          <a href="http://github-media-downloads.s3.amazonaws.com/GitHub_Logos.zip" class="minibutton btn-download download"><span><span class="icon"></span>Download</span></a>
        </li>
        <li>
          <h4>The Octocat</h4>
          <a href="http://github-media-downloads.s3.amazonaws.com/Octocats.zip"><img alt="Octocat" src="https://a248.e.akamai.net/assets.github.com/images/modules/about_page/octocat.png?1329920549" /></a>
          <a href="http://github-media-downloads.s3.amazonaws.com/Octocats.zip" class="minibutton btn-download download"><span><span class="icon"></span>Download</span></a>
        </li>
      </ul>
    </div>

    
    
    
    <span id='server_response_time' data-time='0.05271' data-host='fe15'></span>
  </body>
</html>

