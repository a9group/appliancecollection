TARGETS = boot.udev boot.rootfsck boot.clock boot.device-mapper boot.loadmodules boot.localnet boot.md boot.lvm boot.localfs boot.apparmor boot.udev_retry boot.proc boot.cleanup boot.sysctl boot.fuse boot.swap boot.ipconfig boot.ldconfig boot.klog
INTERACTIVE = boot.rootfsck boot.clock boot.localfs
boot.rootfsck: boot.udev
boot.clock: boot.rootfsck
boot.device-mapper: boot.rootfsck boot.udev
boot.loadmodules: boot.udev
boot.localnet: boot.rootfsck
boot.md: boot.rootfsck boot.udev
boot.lvm: boot.md boot.device-mapper boot.rootfsck boot.udev
boot.localfs: boot.lvm boot.md boot.loadmodules boot.clock boot.rootfsck boot.udev
boot.apparmor: boot.localfs boot.cleanup
boot.udev_retry: boot.localfs boot.rootfsck
boot.proc: boot.localfs boot.rootfsck
boot.cleanup: boot.localfs boot.rootfsck
boot.sysctl: boot.localfs
boot.fuse: boot.localfs boot.udev
boot.swap: boot.localfs boot.lvm boot.md boot.rootfsck
boot.ipconfig: boot.sysctl
boot.ldconfig: boot.clock boot.swap
boot.klog: boot.localfs boot.rootfsck
