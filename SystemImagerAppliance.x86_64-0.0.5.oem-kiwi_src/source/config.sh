#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$name]..."

#======================================
# SuSEconfig
#--------------------------------------
echo "** Running suseConfig..."
suseConfig

echo "** Running ldconfig..."
/sbin/ldconfig

#======================================
# Setup default runlevel
#--------------------------------------
baseSetRunlevel 3

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey


#======================================
# Firewall Configuration
#--------------------------------------
echo '** Configuring firewall...'
chkconfig SuSEfirewall2_init on
chkconfig SuSEfirewall2_setup on

#======================================
# RPM GPG Keys Configuration
#--------------------------------------
echo '** Importing GPG Keys...'
rpm --import /studio/studio_rpm_key_0
rm /studio/studio_rpm_key_0

sed --in-place -e 's/# solver.onlyRequires.*/solver.onlyRequires = true/' /etc/zypp/zypp.conf

# Enable sshd
chkconfig sshd on

#======================================
# Sysconfig Update
#--------------------------------------
echo '** Update sysconfig entries...'
baseUpdateSysConfig /etc/sysconfig/keyboard KEYTABLE us.map.gz
baseUpdateSysConfig /etc/sysconfig/network/config FIREWALL yes
baseUpdateSysConfig /etc/init.d/suse_studio_firstboot NETWORKMANAGER no
baseUpdateSysConfig /etc/sysconfig/SuSEfirewall2 FW_SERVICES_EXT_TCP 22\ 80\ 443
baseUpdateSysConfig /etc/sysconfig/console CONSOLE_FONT lat9w-16.psfu


#======================================
# Setting up overlay files 
#--------------------------------------
echo '** Setting up overlay files...'
mkdir -p /etc/init.d/
mv /studio/overlay-tmp/files//etc/init.d//.depend.boot /etc/init.d//.depend.boot
chown root:root /etc/init.d//.depend.boot
chmod 644 /etc/init.d//.depend.boot
mkdir -p /etc/init.d/
mv /studio/overlay-tmp/files//etc/init.d//.depend.halt /etc/init.d//.depend.halt
chown root:root /etc/init.d//.depend.halt
chmod 644 /etc/init.d//.depend.halt
mkdir -p /etc/init.d/
mv /studio/overlay-tmp/files//etc/init.d//.depend.start /etc/init.d//.depend.start
chown root:root /etc/init.d//.depend.start
chmod 644 /etc/init.d//.depend.start
mkdir -p /etc/init.d/
mv /studio/overlay-tmp/files//etc/init.d//.depend.stop /etc/init.d//.depend.stop
chown root:root /etc/init.d//.depend.stop
chmod 644 /etc/init.d//.depend.stop
mkdir -p /etc/sysconfig/network/
mv /studio/overlay-tmp/files//etc/sysconfig/network//ifcfg-eth0 /etc/sysconfig/network//ifcfg-eth0
chown root:root /etc/sysconfig/network//ifcfg-eth0
chmod 644 /etc/sysconfig/network//ifcfg-eth0
test -d /studio || mkdir /studio
cp /image/.profile /studio/profile
cp /image/config.xml /studio/config.xml
rm -rf /studio/overlay-tmp
true

#======================================
# SSL Certificates Configuration
#--------------------------------------
echo '** Rehashing SSL Certificates...'
c_rehash
