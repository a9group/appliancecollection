# README #

This is the A9 Appliance Collection for SUSE Expert Days 2015

### What's in this Repository? ###

* [z/Dev](https://susestudio.com/a/LVy15n/z-dev-11-4)![zdev.png](https://bitbucket.org/repo/Rjj8AL/images/2208313540-zdev.png)

* [AtlasHA](http://atlassian.a9group.net/) ![atlasplayground.png](https://bitbucket.org/repo/Rjj8AL/images/1917746449-atlasplayground.png)

* [DBOX](https://susestudio.com/a/LVy15n/dbox) ![dbox.png](https://bitbucket.org/repo/Rjj8AL/images/816787355-dbox.png)

* [SAMBHA](http://sambha.a9group.net) ![sambha.png](https://bitbucket.org/repo/Rjj8AL/images/3472844518-sambha.png)

* [Taper](https://bitbucket.org/a9group/appliancecollection/src/abb171ad37e31a7b803828e57e89b299ff537ef6/Taper.i686-0.0.5.oem-kiwi_src/) - Linux audio recording appliance

### How do I get set up? ###

* Prepare a Kiwi Build environment: https://hub.docker.com/r/opensuse/kiwi/
* Clone this repository
* Build the corresponding Kiwi image
* Test in your preferred virtualization environment