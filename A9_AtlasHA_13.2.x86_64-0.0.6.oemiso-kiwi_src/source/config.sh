#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$name]..."

#======================================
# SuSEconfig
#--------------------------------------
echo "** Running suseConfig..."
suseConfig

echo "** Running ldconfig..."
/sbin/ldconfig

#======================================
# Setup default runlevel
#--------------------------------------
baseSetRunlevel 3

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey


sed --in-place -e 's/# solver.onlyRequires.*/solver.onlyRequires = true/' /etc/zypp/zypp.conf

# Enable sshd
chkconfig sshd on

#======================================
# Sysconfig Update
#--------------------------------------
echo '** Update sysconfig entries...'
baseUpdateSysConfig /etc/sysconfig/keyboard KEYTABLE us.map.gz
baseUpdateSysConfig /etc/sysconfig/network/config FIREWALL no
baseUpdateSysConfig /etc/init.d/suse_studio_firstboot NETWORKMANAGER no
baseUpdateSysConfig /etc/sysconfig/SuSEfirewall2 FW_SERVICES_EXT_TCP 22\ 80\ 443
baseUpdateSysConfig /etc/sysconfig/console CONSOLE_FONT lat9w-16.psfu
baseUpdateSysConfig /etc/sysconfig/displaymanager DISPLAYMANAGER_AUTOLOGIN atlas


#======================================
# Setting up overlay files 
#--------------------------------------
echo '** Setting up overlay files...'
xargs -L 256 chown nobody:nobody < /image/archive-manifest-U8dLuREm.txt
mkdir -p /opt/AtlasHA/
mv /studio/overlay-tmp/files//opt/AtlasHA//AtlasHA-tools.zip /opt/AtlasHA//AtlasHA-tools.zip
chown nobody:nobody /opt/AtlasHA//AtlasHA-tools.zip
chmod 644 /opt/AtlasHA//AtlasHA-tools.zip
echo mkdir -p /usr/local/
mkdir -p /usr/local/
echo tar xfp /image/1cd7d631c3ad01382195be356f7aca07 -C /usr/local/
tar xfp /image/1cd7d631c3ad01382195be356f7aca07 -C /usr/local/
echo rm /image/1cd7d631c3ad01382195be356f7aca07
rm /image/1cd7d631c3ad01382195be356f7aca07
mkdir -p /usr/local/
mv /studio/overlay-tmp/files//usr/local//firefox-36.0a2.en-US.linux-x86_64.tar.bz2 /usr/local//firefox-36.0a2.en-US.linux-x86_64.tar.bz2
chown nobody:nobody /usr/local//firefox-36.0a2.en-US.linux-x86_64.tar.bz2
chmod 644 /usr/local//firefox-36.0a2.en-US.linux-x86_64.tar.bz2
mkdir -p /root/GNUstep/Library/WindowMaker/
mv /studio/overlay-tmp/files//root/GNUstep/Library/WindowMaker//autostart /root/GNUstep/Library/WindowMaker//autostart
chown root:root /root/GNUstep/Library/WindowMaker//autostart
chmod 644 /root/GNUstep/Library/WindowMaker//autostart
mkdir -p /etc/
mv /studio/overlay-tmp/files//etc//issue.net /etc//issue.net
chown nobody:nobody /etc//issue.net
chmod 644 /etc//issue.net
mkdir -p /etc/
mv /studio/overlay-tmp/files//etc//motd /etc//motd
chown root:root /etc//motd
chmod 644 /etc//motd
mkdir -p /root/GNUstep/Defaults/
mv /studio/overlay-tmp/files//root/GNUstep/Defaults//WindowMaker /root/GNUstep/Defaults//WindowMaker
chown root:root /root/GNUstep/Defaults//WindowMaker
chmod 644 /root/GNUstep/Defaults//WindowMaker
mkdir -p /etc/sysconfig/
mv /studio/overlay-tmp/files//etc/sysconfig//windowmanager /etc/sysconfig//windowmanager
chown root:root /etc/sysconfig//windowmanager
chmod 644 /etc/sysconfig//windowmanager
mkdir -p /root/GNUstep/Defaults/
mv /studio/overlay-tmp/files//root/GNUstep/Defaults//WMRootMenu /root/GNUstep/Defaults//WMRootMenu
chown root:root /root/GNUstep/Defaults//WMRootMenu
chmod 644 /root/GNUstep/Defaults//WMRootMenu
mkdir -p /root/GNUstep/Defaults/
mv /studio/overlay-tmp/files//root/GNUstep/Defaults//WMWindowAttributes /root/GNUstep/Defaults//WMWindowAttributes
chown root:root /root/GNUstep/Defaults//WMWindowAttributes
chmod 644 /root/GNUstep/Defaults//WMWindowAttributes
test -d /studio || mkdir /studio
cp /image/.profile /studio/profile
cp /image/config.xml /studio/config.xml
chown root:root /studio/build-custom
chmod 755 /studio/build-custom
# run custom build_script after build
if ! /studio/build-custom; then
    cat <<EOF

*********************************
/studio/build-custom failed!
*********************************

EOF

    exit 1
fi
chown root:root /studio/suse-studio-custom
chmod 755 /studio/suse-studio-custom
rm -rf /studio/overlay-tmp
true

#======================================
# SSL Certificates Configuration
#--------------------------------------
echo '** Rehashing SSL Certificates...'
c_rehash
