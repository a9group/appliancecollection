**A9 Consulting Group - Atlas Playground Operating Guide**

![](AtlasHA-Manual_html_m40aa2453.png)

 
AtlasHA was created as a solution to the problem of providing a highly-
available Atlassian Development environment and services on Linux in a
virtualized environment.

  

# Overview
 
  
AtlasHA provides the Atlassian SDK and supporting services including SMB
volume shares and DRBD in an Active/Passive HA configuration.

  
LinuxHA provides the HA stack for the project, and Heartbeat is used to drive
the HA resources. Among these are management of the DRBD volume for the
Atlassian SDK, a VIP to serve the filesystem and the smbd daemon.

  
DRBD provides a distributed filesystem for Atlassian Tools, and takes
advantage of LVM to provide flexible volumes.

  
Fencing is not a component of this implementation.

  
  

# BitBucket

Access the BitBucket project at

[https://bitbucket.org/a9group/AtlasHA](https://github.com/openlpos/SAMBHA)

Working with Git is beyond the scope of this document. Follow your preferred
workflow to clone the repository locally with 'git clone' or use BitBucket's
interface to fork the Master branch.

No formal style manual exists. One may emerge, but there is no expectation for
public contributions to co-exist from a style perspective.

The BitBucket sources may be used to build a Debian version of the appliance,
using your preferred Debian bootstrapping procedure; debbootstrap, network
installation, or as an overlay to a pre-configured VM.

# **SUSE Studio and Kiwi**

We took advantage of SUSE Studio to rapidly deploy the basic Linux operating
system, including the necessary prerequisites, per Atlassian's guidelines.

A byproduct of Studio is the Kiwi configuration file that can be used to
rebuild derivative appliances, or modify the base build configuration to build
iterations of an existing image.

## Deployment

We have broken the deployment operation down to four phases:

  
  

**Phase 1**: The import of the OVF configuration file and backing store. We also perform some basic configuration, like setting up the network and virtual filesystems that will be connected to DRBD.

NOTE: This is done for both servers, Hot and Warm

  
  

**Phase 2**: Configuration of the Server Roles – which server is Active, and which remains Passive.

  
  

**Phase 3**: Testing. We put the machine through a few tests; checking service failover, DRBD control handover, and the basics of fencing.

  
  

**Phase 4:** Creation of a 'utility node'. This third node will allow you to manage the HA cluster and provide a spare environment to be swapped in. It also provides an out-of-cluster environment from which to issue commands.

## Phase 1

#### Import OVF

Import OVF build from SUSE Studio

  
There are several differences between the Studio download and a ready-to-run
AtlasHA deployment. This guide attempts to walk you through the process.

Suse Studio delivers a compressed OVF archive, including the VM description
and OS disk image. Extract this file and import to your hypervisor of choice.

This section is also a good starting point if you need to build additional
AtlasHA pairs for Dev/Test, or to build a Disaster Recovery environment.

**Login**  
  
Login as root with

  
username: root

password: linux

#### Basic Configuration

Network

Three network interfaces should be available:

  1. **Public**  
Link over which the SAMBA service is available. Also serves as ssh and package
update connection.

  2. **DRBD**  
Dedicated internal network specifically for DRBD sync; e.g., 192.168.9.n/24

  3. **Heartbeat**  
Dedicated network for heartbeat communication; e.g., 192.168.10.n/24

Disk

The ideal implementation provides mirrored storage backing the LVM for DRBD.
The current iteration relies on the mirrored volumes for redundancy. There are
multiple paths which can be taken to provide fault tolerance at the block
level:

  
**LVM Mirroring**

Utilize a separate physical device to provide a hardware mirror for LVM.
Requires N+2 disks.

#### LVM automatic snapshots

#####

Incorporate a cron-driven task to create volume snapshots. Recommended
practice for preventing against disk invalidation errors occuring DRBD
synchronizations.

See:&nbsp;_place_holder;[**http://www.drbd.org/users-guide/s-lvm-
snapshots.html**](http://www.drbd.org/users-guide/s-lvm-snapshots.html)

  
**VMware Hardware mirroring**

  
Utilize facilities within VMware for automatic backup and enhanced storage
allocation

  
**Linux Kernel-based Software mirroring**

  
Use mdadm or raidtools to create and manage a software RAID.

Preparing volumes

  
**Allocate within VMware**  
Create or attach a virtual block device to the guest.

  
**Configure within OS**  
AtlasHA appliance expects a set of LVM2 volumes to be available:

  
**Physical Volume**  
This will change depending on the number of disks available to the appliance,
but by default '/dev/sda' is used for the operating system, and '/dev/sdb' as
the dedicated Physical Volume (pv) which is later allocated to the Volume
Group (vg) 'drbdsamba0vg'

To prepare LVM volumes for DRBD, run as root:

    
    sh ha_init.sh

  
From /root directory.

  
Manual:

    
    pvcreate /dev/sdb

#####  **Volume Group**

  
  

This is the disk group upon which the logical volume 'drbdsamba0lv' resides.
The Volume Group may be expanded to include additional capacity incorporated
from additional Physical Volumes or extensions to the PV/PVs already in
use.&nbsp;_place_holder;

Automatic Creation:

The ha_init.sh script will remove any preexisting volumes on the specified
disk. Re-run this script as necessary to build the desired disk onfiguration.
Run as root user from '/root/':

    
    sh ha_init.sh

Manual:

    
    vgcreate drbdsamba0vg /dev/sdb

****Logical Volume****

  
  

The Logical Volume 'drbdsamba0lv' upon which the device for DRBD
(samba0/drbd0) is created.

  
  

Note: The Linux filesystem for SAMBA is created on 'samba0' after preparation
and presentation to the OS through DRBD.

  
  

    
    **Automatic:**  
    run as root from '/root/':
    sh ha_init.sh

**Manual:**
    
    lvcreate -L 28640M -n drbdsamba0lv drbdsamba0vg

  
**DRBD**  
  
DRBD setup is still a manual process, and must be carried out after creating
or destroying the LVM configuration.

  
Bring up both nodes. Each must be accessible via the DRBD interface as well as
Public for SSH.

  * **Verify the availability of the DRBD device:**
    
    cat /proc/drbd

  * **From 'hot' node, create the DRBD Metadata**  
Run as root:

    
    drbdadm create-md samba0

Also on the 'hot' node, **initialize the syncronization process.**

Run as root:

    
    drbdadm primary all

  * **Verify the status of the DRBD device as Primary/Inconsistant:**

  
  

    
    cat /proc/drbd

#####  Network Time Protocol

  
  

Accurate synchronization between nodes is essential, and requires NTP
configuration.

Remove Local clock entry (127.x.x.x) and replace in /etc/ntp.conf to include
your preferred clock source.

  
  
  

  * **Force clock sync:**
    
    /etc/init.d/ntp restart

  * **Check time**
    
    
    From 'hot', as root, run:
    
    date ; ssh warm date
    

###  Phase 2

####

Configure Roles

  
****Hot****  
Hot role is assumed on the host where 'ha_init.sh' is run. This host may also
be cloned from an OVF export, and reconfigured with ha_init.sh, or with a
manual process to reconfigure the hostname and IP addresses assigned.

This guide gives 'warm' the final octet of .11

  
****Warm****  
Warm role is inherited after preparing a AtlasHA with 'ha_init.sh' and
propagating with 'ha_synchaconfig.sh'

This guide gives 'warm' the final octet of .12

  
  

### Phase 3

####

Test heartbeat

Ensure the heartbeat daemon process is running

    
    service heartbeat status

  
Should read 'running'

####

Test DRBD

Ensure the DRBD module is loaded

    
    lsmod | grep drbd

####

Ensure the DRBD process is running

    
    service drbd status

  
Should read 'running'

#####

Test SAMBA

Ensure the SMB process is running

    
    service smb status

  
Should read 'running'

### Clone Hot node to Warm

  
AtlasHA needs a hot and warm node to function.

  
Clone the configured hot VM, including the volumes attached for DRBD.

  
Boot in single-user mode by issuing

    
    single

at the GRUB prompt. This will bring the system up for a single user, without
activating the network interfaces.

  
Run

    
    yast2 lan

This provides a console interface to reconfigure the each of the assigned
network interfaces for DRBD, Heartbeat, and the Public network.

  
The hostname must also be changed at this time. Find this by selecting the
'Hostname' tab in Yast.

  
Reboot the new warm node, and test connectivity from the hot node via ssh
connection to each interface.

  
This process should also be followed to create a Util node for testing HA
failover. This node may also be configured to provide monitoring with Nagios.

  
The [**'ha_hatest.sh'**](http://www.sambha.openlpos.org/static.php?page=static
120512-161944) script can be used to perform validation testing at this stage
from the resulting Utility node.

Download from BitBucket: [https://github.com/juddy/AtlasHA/tree/master/overlay
/util/ha_hatest.sh](https://github.com/juddy/SAMBHA/tree/master/overlay/util/f
s_hatest.sh)

#### Test failover

  
  
This procedure assumes the Warm node has been populated by following the Clone
procedures outlined above.

From the Util node, execute the ha_hatest.sh script, available on in the
AtlasHA project on BitBucket, here: [http://bitbucket.org/a9group/AtlasHA/ha_h
atest.sh](https://github.com/juddy/SAMBHA/blob/master/overlay/util/fs_hatest.s
h)

  
Webmin may also be used to take control of Heartbeat's resources with the
Heartbeat module, under the “Cluster” panel.

  
Manually forcing failover may be done from any node, using the following
methods:

#### Standby:

Force the current host to submit it's resources to the opposing host.

As root:

    
    /usr/share/heartbeat/hb_standby ; tail -f /var/log/ha-log

This should show the transfer of control of the HA resources to the other
node.

### Phase 4

  
Create Utility Node

  
A utility and test node cloned from a 'warm' host can be used to monitor and
test the performance of AtlasHA.

  
A test application is available to test failover through a number of
processes; 'ha_hatest.sh'

  
Run ha_hatest.sh from a Utility node to perform a suite of tests to verify HA
operability.

The script seen below (ha_hatest.sh on util) performs three tests on each
host: a forced reboot, a forced failover, and forced failback.
&nbsp;_place_holder;I have run it in a few different permutations, and you can
see several functions that can be called as tests; force_reboot,
force_failover, stop_heartbeat, force_disconnect, force_standby, etc.

The execution mechanism runs through 30 calls to one host (beginning with
hot), performing one of the tests at a specified interval, currently set as
10, 20, and 25 seconds. &nbsp;_place_holder;Every 30 seconds, three pieces of
data are written to a file in /mnt on util, which is the SAMBA share.
&nbsp;_place_holder;A timestamp, a marker, and the name of the test being
executed (if any). This logfile is written as 'ha-
test.out.YYYYmmdd'.

[BitBucket link here](bitbucket.org/a9group/AtlasHA/utils/ha_hatest.sh)
  

#### Support and Management

Disaster recovery, routine backup, and dev-test-prod workflows may be achieved
through the rotation of system snapshots managed in a number of ways:

  * Routine Hypervisor-level exports

  * Rsync-driven replication from within AtlasHA

  * Kiwi-driven replication

  * SystemImager replication

This guide assumes a block-level replicant is available for reassignment as a
utility or replacement node for a hot or warm failure, or in the event that an
additional pair should be made available. The steps are essentially the same
in each case. This guide uses example static IPv4 address assignment schemes,
and should be substituted for a network topology suited to your environment.

A flowchart to the appropriate processes described in this guide is presented
below. It may be used to create a 'Util' node, replace a failed node, or as a
checklist to prepare and verify operation of an expanded AtlasHA
implementation.

  
The differences between 'hot' and 'warm nodes are minimal. Managing AtlasHA
expansion through images ensures a rapid DR time and flexibility between
platforms and infrastructures.The Hot node should be used as primary point of
interaction, pushing configuration changes to warm node via the builtin
'ha_synchaconfig.sh'.

    
    drbdadm primary all

#####  Manual Reassignment Process Outlined

  
  

Assumes a cloned VM is available, and

  1. **Change hostname**  
As root:

    
    hostname warm

  
**Note: The same applies to changing to 'hot' from warm. Use the opposing member.**

  1. Check the /etc/hostname file to ensure the correct data; correct IP address for hostname – correcting for MAC address changes.

  2. Change interface IP addresses, verify hostname in YaST2 Hostname tab.  
As root, run:

    
    yast2 lan

  3. Reconfigure each of the interfaces to use the correct IP address for the role.

  
**Hot**:

      1. DRBD 192.168.10.11

      2. Heartbeat192.168.9.11 

**Warm**:

      1. DRBD192.168.10.12

      2. Heartbeat192.168.9.12

**Alternately, edit the interface definition files in /etc/sysconfig/network/ifcfg-eth0**  
  
  

  4. Verify 

    1. Verify Heartbeat Resource availability

    2. Verify DRBD status  
As root: run

    
     drbdadm status

    3. Verify VIP status  
As root run

    
    ifconfig eth0:1

    4. Verify correct SAMBA service IP address is configured  
Verify SAMBA status

As root: run

    
    smbstatus

#####  Monitoring

  
Each node has Nagios installed. Suggested configuration involves active SMB
checks on the opposing node and verification that the expected /srv/samba
mount for /dev/drbd0 is available

Configuring other monitoring tools for monitoring AtlasHA

Verify SMB service availability on SAMBA VIP

##### Managing SAMBA data store

  
AtlasHA manages the DRBD device made available through Heartbeat as a resource
for Heartbeat.

  
No manual mount operations are required.

  
If the filesystem must be made available to the Operating System outside of
Heartbeat's control, you must do the following:

    
    /etc/init.d/heartbeat stop

  
  
With DRBD:

    
    mount /dev/drbd0 /srv/samba

  
  
Without DRBD:

    
    mount /dev/drbdsamba0vg/drbdsamba0lv /srv/samba

  
  
If this fails, be sure the block device housing the Physical Volumes for each
of the volumes in the group in which the drbdsamba0lv volume exists

  
For assistance with raw disk outside of DRBD control, contact your support
contact for AtlasHA or [william@a9group.net](mailto://william@a9group.net)

  

####

Modifying Heartbeat Resources

AtlasHA's Heartbeat resource file is located in /etc/ha.d/samba.conf