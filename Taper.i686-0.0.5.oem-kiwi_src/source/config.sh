#!/bin/bash
#================
# FILE          : config.sh
#----------------
# PROJECT       : OpenSuSE KIWI Image System
# COPYRIGHT     : (c) 2006 SUSE LINUX Products GmbH. All rights reserved
#               :
# AUTHOR        : Marcus Schaefer <ms@suse.de>
#               :
# BELONGS TO    : Operating System images
#               :
# DESCRIPTION   : configuration script for SUSE based
#               : operating systems
#               :
#               :
# STATUS        : BETA
#----------------
#======================================
# Functions...
#--------------------------------------
test -f /.kconfig && . /.kconfig
test -f /.profile && . /.profile

#======================================
# Greeting...
#--------------------------------------
echo "Configure image: [$name]..."

#======================================
# SuSEconfig
#--------------------------------------
echo "** Running suseConfig..."
suseConfig

echo "** Running ldconfig..."
/sbin/ldconfig

#======================================
# Setup default runlevel
#--------------------------------------
baseSetRunlevel 5

#======================================
# Add missing gpg keys to rpm
#--------------------------------------
suseImportBuildKey


sed --in-place -e 's/# solver.onlyRequires.*/solver.onlyRequires = true/' /etc/zypp/zypp.conf

# Enable sshd
chkconfig sshd on

#======================================
# Sysconfig Update
#--------------------------------------
echo '** Update sysconfig entries...'
baseUpdateSysConfig /etc/sysconfig/keyboard KEYTABLE us.map.gz
baseUpdateSysConfig /etc/sysconfig/network/config FIREWALL no
baseUpdateSysConfig /etc/init.d/suse_studio_firstboot NETWORKMANAGER yes
baseUpdateSysConfig /etc/sysconfig/SuSEfirewall2 FW_SERVICES_EXT_TCP 22\ 80\ 443
baseUpdateSysConfig /etc/sysconfig/console CONSOLE_FONT lat9w-16.psfu
baseUpdateSysConfig /etc/sysconfig/displaymanager DISPLAYMANAGER_AUTOLOGIN taper


#======================================
# Setting up overlay files 
#--------------------------------------
echo '** Setting up overlay files...'
xargs -L 256 chown nobody:nobody < /image/archive-manifest-8KeB2pCT.txt
mkdir -p /
mv /studio/overlay-tmp/files///master.zip //master.zip
chown nobody:nobody //master.zip
chmod 644 //master.zip
mkdir -p /usr/local/bin/
mv /studio/overlay-tmp/files//usr/local/bin//enable_touchscreen /usr/local/bin//enable_touchscreen
chown root:root /usr/local/bin//enable_touchscreen
chmod 775 /usr/local/bin//enable_touchscreen
mkdir -p /etc/skel/.vimrc/
mv /studio/overlay-tmp/files//etc/skel/.vimrc//vimrc /etc/skel/.vimrc//vimrc
chown nobody:nobody /etc/skel/.vimrc//vimrc
chmod 644 /etc/skel/.vimrc//vimrc
mkdir -p /etc/skel/.Xdefaults/
mv /studio/overlay-tmp/files//etc/skel/.Xdefaults//Xdefaults /etc/skel/.Xdefaults//Xdefaults
chown nobody:nobody /etc/skel/.Xdefaults//Xdefaults
chmod 644 /etc/skel/.Xdefaults//Xdefaults
test -d /studio || mkdir /studio
cp /image/.profile /studio/profile
cp /image/config.xml /studio/config.xml
chown root:root /studio/build-custom
chmod 755 /studio/build-custom
# run custom build_script after build
if ! /studio/build-custom; then
    cat <<EOF

*********************************
/studio/build-custom failed!
*********************************

EOF

    exit 1
fi
chown root:root /studio/suse-studio-custom
chmod 755 /studio/suse-studio-custom
rm -rf /studio/overlay-tmp
true

#======================================
# SSL Certificates Configuration
#--------------------------------------
echo '** Rehashing SSL Certificates...'
c_rehash
